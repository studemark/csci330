#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct Card {
   char suit;
   char rank;
   int val;
} Card;

typedef enum handType {
   N, NR, NA, AS, AP
} handType;

typedef struct Hand { 
   Card cards[5];
   handType type;
   int wonTie;
} Hand;

char* makeHand(char *curLine, Hand *hand) {
   Hand curHand;
   Card newCard;
   //printf("\n%s\n", curLine);
   for (int i = 0; i < 5; i++) {
      while (isspace(*curLine))
         curLine++;
      if (strchr("A23456789TJQK", toupper(*curLine)))
         newCard.rank = toupper(*curLine);
      else {
         printf("%s\n", "Bad hands");
         return NULL;
      }
      if (strchr("SDHC\0", toupper(*(curLine + 1))))
         newCard.suit = toupper(*(curLine + 1));
      else {
         printf("%s\n", "Bad hands");
         return NULL;
      }
      curHand.cards[i] = newCard;
      if ((*(curLine + 2) == '\0' || *(curLine + 3) == '\0') && i < 4) {
         printf("%s\n", "Bad hands");
         return NULL;
      }
      if (isalpha(*(curLine + 2))) {
         printf("%s\n", "Bad hands");
         return NULL;
      }
      else 
         curLine += 3;
   }
   *hand = curHand;
   return curLine;
}

void fillVals(Hand *hand) {
   char *ranks = "A23456789TJQK";

   for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 13; j++) {
         if (ranks[j] == hand->cards[i].rank) {
            hand->cards[i].val = j+1;
            break;
         }
      }
   }
}

void printHand(Hand hand) {
//printf("Printing hand: ");
   for (int i = 0; i < 5; i++) {
// CAS: Cast shouldn't be needed.
      Card curCard = (Card)hand.cards[i];
      printf("%c", curCard.rank);
      printf("%c ", curCard.suit);
      //printf("%d ", curCard.val);
   }
   if (hand.type == 0)
      printf("%s", "Nothing");
   if (hand.type == 1)
      printf("%s", "No Repeats");
   if (hand.type == 2)
      printf("%s", "No Adjacencies");
   if (hand.type == 3)
      printf("%s", "All Suits");
   if (hand.type == 4)
      printf("%s", "All Primes");
   //printf(hand.handType);
}

void Swap(Card *c1, Card *c2) {
   Card temp = *c1;
   *c1 = *c2;
   *c2 = temp;
}

// CAS Passing a char pointer to this method will not let it modify the
// original char pointer, nor skip any space.
void SortHand(Hand *handToSort) {
   Card *next = handToSort->cards, *pLoc, *valsEnd = handToSort->cards + 5;
   Card *toInsert;

   for (; next < valsEnd; next++) {
      toInsert = next;
      pLoc = next;
      while (pLoc > handToSort->cards && (toInsert->val < (pLoc - 1)->val)) {
         Swap(pLoc, pLoc-1);
         next = handToSort->cards;
      }                                
   } 
}

void EvalHand(Hand *handToEval) {
   int repeat = 0, adj = 0, suitCounter = 0, primeCounter = 0;
   char suitStr[] = "SHDC";
   char rankStr[] = "2357JK";
   char *match;
   handToEval->type = N;
   for (int i = 0; i < 5; i++) {
      if (i < 4) {
         if (handToEval->cards[i].val == handToEval->cards[i+1].val) {
            repeat = 1;
         }
         //printf("%d", handToEval->cards[i].val);
         //printf("%d ", ((handToEval->cards[i+1].val) - 1));
         if (handToEval->cards[i].val == ((handToEval->cards[i+1].val) - 1)) {
            adj = 1;
         }
      }
      if (strchr(suitStr, handToEval->cards[i].suit)) {
         match = strchr(suitStr, handToEval->cards[i].suit);
         *match = '*';
         suitCounter++;
      }
      if (strchr(rankStr, handToEval->cards[i].rank)) {
         primeCounter++;
      }
   }
   if (repeat == 0)
      handToEval->type = NR;
   if (adj == 0 && repeat == 0)
      handToEval->type = NA;
   if (suitCounter >= 4)
      handToEval->type = AS;
   if (primeCounter == 5)
      handToEval->type = AP;
}

void CompHands (Hand *h1, Hand *h2) {
   int h1Count = 0;
   int h2Count = 0;
   h1->wonTie = 0;
   h2->wonTie = 0;
   if (h1->type > h2->type) {
      printHand(*h1);
      printf("%s", " beats ");
      printHand(*h2);
      printf("\n");
   }
   if (h2->type > h1->type) {
      printHand(*h2);
      printf("%s", " beats ");
      printHand(*h1);
      printf("\n");
   }
   if (h1->type == h2->type) {
      if (h1->type == AS) {
         for (int i = 0; i < 5; i++) {
            h1Count += (int)h1->cards[i].suit;
            h2Count += (int)h2->cards[i].suit;
         }
         if (h1Count > h2Count) {
            h1->wonTie = 1;
         }
         if (h2Count > h1Count) {
            h2->wonTie = 1;
         }
      }
      if (h1->wonTie == 0 && h2->wonTie == 0) {
         for (int i = 4; i >= 0; i--) {
            if (h1->cards[i].val > h2->cards[i].val) {
               h1->wonTie = 1;
               break;
            }
            if (h2->cards[i].val > h1->cards[i].val) {
               h2->wonTie = 1;
               break;
            }
         }
      }
      if (h1->wonTie == 1) {
         printHand(*h1);
         printf("%s", " beats ");
         printHand(*h2);
         printf("\n");
      } 
      else if (h2->wonTie == 1) {
         printHand(*h2);
         printf("%s", " beats ");
         printHand(*h1);
         printf("\n");
      }
      else {
         printHand(*h1);
         printf("%s", " ties ");
         printHand(*h2);
         printf("\n");
      }
   }
}

int main() {
   char line[256];
   FILE *file = fopen("input2.txt", "r");
   while (fgets(line, sizeof(line), file)) {
   char *linePtr = line;
      while (*linePtr && isspace(*linePtr))
         linePtr++;
      if (*linePtr) {
         Hand newHand;
         char *newLine = makeHand(linePtr, &newHand);
         if (newLine) {
            Hand nxtHand;
            if (makeHand(newLine, &nxtHand)) {
               fillVals(&newHand);
               SortHand(&newHand);
               EvalHand(&newHand);
         
               fillVals(&nxtHand);
               SortHand(&nxtHand);
               EvalHand(&nxtHand);
         
               CompHands(&newHand, &nxtHand);
            }
         }
      }
   }
   return 0;
}
