#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include "ReadSuite.h"

int main(int argc, char **argv) {
   char line[256];
   char *suiteName = argv[1];
   char *tokLine;
   FILE *file = fopen(suiteName, "r");
   while (fgets(line, sizeof(line), file)) {
      char *linePtr = line;
      tokLine = strtok(linePtr, "\n");
      printf("%s", tokLine);
      printf("\n");
   }

   //readSuite(fp);

   return 0;
}