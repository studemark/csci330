#ifndef SUITE_H_INCLUDED_
#define SUITE_H_INCLUDED_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

// A single test comprising input, output, time limit, and arguments
typedef struct Test {
   char *inFile;
   char *outFile;

   int msTimeLimit;

   int argc;
   char **argv;
} Test;

// A single Program, including executable name, and list of source files
typedef struct Program {
   char *name;

   std::vector<char *> sourceFiles;
   std::vector<Test *> tests;
} Program;

// A suite of programs to build and run
typedef struct Suite {
   std::vector<Program *> programs;
} Suite;

#endif
