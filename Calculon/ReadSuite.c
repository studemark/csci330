#include "ReadSuite.h"

void readSuite(FILE *suite) {
   char test;
   char str[256];

   fgets(str, 256, suite);

   while(test = getc(suite)) {
      if (test == 'T') {
         testNum++;
         fgets(str, MAX_STR_LEN, suite);
      }
      else {
         ungetc(test, suite);
         break;
      }
   }
}